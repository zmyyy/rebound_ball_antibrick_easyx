#include <graphics.h>
#include <conio.h>
#include <stdlib.h>
#include <time.h>
#define High 480  // 游戏画面尺寸
#define Width 640

// 全局变量
int ball_x,ball_y; // 小球的坐标
int ball_vx,ball_vy; // 小球的速度
int radius;  // 小球的半径
int board_x;
int board_y;
int board_long;
int board_bottom;
int gaol_x,gaol_y,gaol_long;
void startup()  // 数据初始化
{
	ball_x = Width/2;
	ball_y = High/2;
	ball_vx = 1;
	ball_vy = 1;
	radius = 20;
	board_x=Width/2;
	board_y=High/2;
	board_long=150;
	board_bottom=30;

	gaol_y=2;
    gaol_x=Width/2;
    gaol_long=20;  
	
	initgraph(Width, High);
	BeginBatchDraw();
}

void clean()  // 显示画面
{
	// 绘制黑线、黑色填充的圆
	setcolor(BLACK);
	setfillcolor(BLACK);
	fillcircle(ball_x, ball_y, radius);
}	

void show()  // 显示画面
{
	setbkcolor(RGB(255,255,255));
    cleardevice();
	// 绘制黄线、绿色填充的圆
	setcolor(YELLOW);
	setfillcolor(GREEN);
	fillcircle(ball_x, ball_y, radius);	
    setfillcolor(BLUE);
    fillrectangle(board_x,board_y,board_x+board_long,board_y+board_bottom);

	setfillcolor(RGB(255,0,0));
	fillrectangle(gaol_x,gaol_y,gaol_x+gaol_long,gaol_y+gaol_long);
	  
	FlushBatchDraw();
	// 延时
	Sleep(3);	
}	

void updateWithoutInput()  // 与用户输入无关的更新
{
	// 更新小圆坐标
	ball_x = ball_x + ball_vx;
	ball_y = ball_y + ball_vy;
	
	if ((ball_x<=radius)||(ball_x>=Width-radius))
		ball_vx = -ball_vx;
	if ((ball_y<=radius)||(ball_y>=High-radius))
		ball_vy = -ball_vy;	
	if(ball_y+radius>board_y&&(ball_x>=board_x&&ball_x<board_x+board_long))//碰木板反弹 
        ball_vy=-ball_vy;
    if(ball_y==board_y+board_bottom&&(ball_x>=board_x&&ball_x<board_x+board_long))//碰木板反弹 
        ball_vy=-ball_vy;
if((ball_x>gaol_x&&ball_x<gaol_x+gaol_long)&&(ball_y>gaol_y&&ball_y<gaol_y+gaol_long))//如果球击中目标
    {
        srand((unsigned)time(NULL));//做随机数产生种子
       gaol_y=rand()%(High/2-gaol_long)+1;
        gaol_x=rand()%(Width/2-gaol_long)+1;
    }
	
}

void updateWithInput()  // 与用户输入有关的更新
{	
	char input;
    if(kbhit())
    {
        input=getch();
        if(input=='w'&&board_y>1)
            board_y-=10;
        if(input=='s'&&board_y+board_bottom<High-2)
            board_y+=10;
        if(input=='a'&&board_x>1)
            board_x-=10;
        if(input=='d'&&board_x+board_long<Width-2)
            board_x+=10;
    } 
}

void gameover()
{
	EndBatchDraw();
	closegraph();
}

int main()
{
	startup();  // 数据初始化	
	while (1)  //  游戏循环执行
	{
		clean();  // 把之前绘制的内容清除
		updateWithoutInput();  // 与用户输入无关的更新
		updateWithInput();     // 与用户输入有关的更新
		show();  // 显示新画面
	}
	gameover();     // 游戏结束、后续处理
	return 0;
}










